package animal;

public class Main {
    public static void main(String[] args) {
        Duck duck = new Duck(12, "male", "yellow");
        Fish fish = new Fish(5, "female", 20, true);
        Zebra zebra = new Zebra(10, "male", 2, true);
        System.out.println("Duck" + duck.getGender() + duck.getAge() + duck.getBeakColor());
    }

}
