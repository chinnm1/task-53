package animal;

public class Duck extends Animal {

    public String beakColor;

    public Duck() {
    }

    public Duck(int age, String gender, String beakColor) {
        this.age = age;
        this.gender = gender;
        this.beakColor = beakColor;
    }

    public void swim() {

    }

    public void quack() {

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getBeakColor() {
        return beakColor;
    }

    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }

}
