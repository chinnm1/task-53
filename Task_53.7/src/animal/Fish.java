package animal;

public class Fish extends Animal {

    public int size;
    boolean canEat;

    public Fish(int age, String gender, int size, boolean canEat) {
        this.age = age;
        this.gender = gender;
        this.size = size;
        this.canEat = canEat;
    }

    public Fish() {
    }

    public void swim() {

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isCanEat() {
        return canEat;
    }

    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }

}
