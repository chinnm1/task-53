package animal;

public class Zebra extends Animal {

    public int size;
    boolean is_wild;

    public Zebra(int age, String gender, int size, boolean is_wild) {
        this.age = age;
        this.gender = gender;
        this.size = size;
        this.is_wild = is_wild;
    }

    public Zebra() {
    }

    public void run() {

    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public boolean isIs_wild() {
        return is_wild;
    }

    public void setIs_wild(boolean is_wild) {
        this.is_wild = is_wild;
    }
}
