package com.devcamp.j04_javabasic.s10;

public class CPet extends CAnimal {
    protected int age;
    protected String name;

    @Override
    public void animalSound() {
        // TODO Auto-generated method stub
        System.out.println("Pet sound...");
    }

    @Override
    public void eat() {
        System.out.println("Pet eating");

    }

    protected void print() {

    }

    protected void play() {

    }

    public static void main(String[] args) {
        CPet myPet = new CPet();
        myPet.name = "KIYa";
        myPet.age = 2;
        myPet.animclass = AnimalClass.mammals;
        myPet.eat();
        myPet.animalSound();
        myPet.print();
        myPet.play();

    }

}
