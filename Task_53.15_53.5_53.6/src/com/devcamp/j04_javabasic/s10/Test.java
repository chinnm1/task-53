package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        CAnimal nameA2 = new CFish();
        CPet name2 = new CBird();
        CPerson namePerson = new CPerson();
        namePerson.setAge(20);
        ArrayList<CPet> petList = new ArrayList<>();
        petList.add(name2);
        petList.add((CPet) nameA2);
        namePerson.setPets(petList);
        System.out.println(petList);
        System.out.println(namePerson.toString());

    }

}
