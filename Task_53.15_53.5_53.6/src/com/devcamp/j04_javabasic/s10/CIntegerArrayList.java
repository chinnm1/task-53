package com.devcamp.j04_javabasic.s10;

import java.util.ArrayList;

import com.devcamp.j04_javabasic.s10.interfaceclass.ISumable;

public class CIntegerArrayList implements ISumable {
    private ArrayList<Integer> mIntegerArrayList;

    public CIntegerArrayList() {
    }

    public CIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
        this.mIntegerArrayList = mIntegerArrayList;
    }

    public double getSumOfArrayList() {
        double sum = 0;
        for (int i = 0; i < mIntegerArrayList.size(); i++) {
            sum += mIntegerArrayList.get(i);
        }
        return sum;
    }

    @Override
    public String getSum() {
        // TODO Auto-generated method stub

        return "Đây là sum của class CIntegerArrayList" + getSumOfArrayList();
    }

    public ArrayList<Integer> getmIntegerArrayList() {
        return mIntegerArrayList;
    }

    public void setmIntegerArrayList(ArrayList<Integer> mIntegerArrayList) {
        this.mIntegerArrayList = mIntegerArrayList;
    }
}
