package com.devcamp.j04_javabasic.s10.interfaceclass;

public interface ISumable {
    String getSum();

}
